"""
This module is an example of a barebones numpy reader plugin for napari.

It implements the ``napari_get_reader`` hook specification, (to create
a reader plugin) but your plugin may choose to implement any of the hook
specifications offered by napari.
see: https://napari.org/docs/dev/plugins/hook_specifications.html

Replace code below accordingly.  For complete documentation see:
https://napari.org/docs/dev/plugins/for_plugin_developers.html
"""
import numpy as np
import pandas as pd
from cryocat import cryomap
from starfile import read as starread
from pathlib import Path

file_endings = (".em", ".mrc", ".rec", ".star")


def napari_get_reader(path):
    """A basic implementation of a Reader contribution.

    Parameters
    ----------
    path : str or list of str
        Path to file, or list of paths.

    Returns
    -------
    function or None
        If the path is a recognized format, return a function that accepts the
        same path or list of paths, and returns a list of layer data tuples.
    """

    # Widgets often return a tuple. If that's the case, turn it into a list.
    if isinstance(path, tuple):
        path = path[0]
    # check if path is a list if not, turn it into one    
    path = [path] if isinstance(path, str) else path

    # check if file exists (only necessary when a path is read from a file)
    if not all(Path(_path).is_file() for _path in path):
        return None
    
    # if we know we cannot read any of the files, we immediately return None.
    if not all(any(filename.endswith(ending) for ending in file_endings) for filename in path):
        return None

    # otherwise we return the *function* that can read ``path``.
    if not path:
        return None
    elif all(any(filename.endswith(ending) for ending in (".em", ".mrc", ".rec")) for filename in path):
        return volume_reader_function
    elif all(filename.endswith(".star") for filename in path):
        return star_reader_function(path)

def volume_reader_function(path):
    """Take a path or list of paths and return a list of LayerData tuples.

    Readers are expected to return data as a list of tuples, where each tuple
    is (data, [add_kwargs, [layer_type]]), "add_kwargs" and "layer_type" are
    both optional.

    Parameters
    ----------
    path : str or list of str
    Path to file, or list of paths.

    Returns
    -------
    layer_data : list of tuples
    A list of LayerData tuples where each tuple in the list contains
    (data, metadata, layer_type), where data is a numpy array, metadata is
    a dict of keyword arguments for the corresponding viewer.add_* method
    in napari, and layer_type is a lower-case string naming the type of layer.
    Both "meta", and "layer_type" are optional. napari will default to
    layer_type=="image" if not provided
    """

    # handle both a string and a list of strings
    path = [path] if isinstance(path, str) else path

    # load all files into array
    arrays = [cryomap.read(_path, transpose=False) for _path in path]
    # stack arrays into single array
    data = np.squeeze(np.stack(arrays))

    # optional kwargs for the corresponding viewer.add_* method.
    add_kwargs = {}

    layer_type = "image"  # optional, default is "image"

    return [(data, add_kwargs, layer_type)]


def star_reader_function(path):
 
    
    # handle both a string and a list of strings
    paths = [path] if isinstance(path, str) else path

    # load all files into a list and concat them vertically
    tm_params = pd.concat([starread(_path) for _path in paths], axis=0, ignore_index=True)
    
    if tm_params is not None:
        return tm_params
    else:
        return None