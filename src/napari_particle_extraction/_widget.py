"""
This module is an example of a barebones QWidget plugin for napari

Replace code below according to your needs.
"""
import gc
from qtpy.QtCore import Qt, Signal
from qtpy.QtGui import QColor, QBrush
from qtpy.QtWidgets import *
import napari
from napari.utils.colormaps import AVAILABLE_COLORMAPS
import os
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.stats import norm as scipynorm
from cryocat import cryomotl

from napari_particle_extraction._reader import napari_get_reader
from napari_particle_extraction._writer import napari_get_writer

class SG_TM_Particle_Extract(QWidget):
    def __init__(self, napari_viewer) -> None:
        super().__init__()
        self.viewer = napari_viewer

        # Retrieve viewer parameters to be used for display
        # Retrieve the current ndisplay, grid mode, dimension order from the viewer
        self.last_ndisplay = self.viewer.dims.ndisplay
        self.last_grid_mode = self.viewer.grid.enabled
        self.current_dim_order = self.viewer.dims.order
        self.viewer_angles = self.viewer.camera.angles
        self.viewer_zoom = self.viewer.camera.zoom
        self.viewer_center = self.viewer.camera.center

        # Initialize tm_params variables
        self.tm_params = None
        self.layer_visibility = {"Tomogram": {"visible": True, "name": None},
                                 "Score": {"visible": True, "name": None},
                                 "Angles": {"visible": False, "name": None}}
        self.score_name = None
        self.tomo_name = None
        self.angles_name = None
        self.current_particles_name = "current_particles"
        self.deleted_particles_name = "deleted_particles"
        self.score_min = 0
        self.score_max = 0
        self.score_mean = None
        self.score_std = None
        self.patches = None
        self.cmin = 0
        self.cmax = 0
        self.hist_cmap = 'viridis'
        self.patch_heights = 0
        self.bins = 0
        # Particles to be displayed
        self.particles = None
        self.particle_count = 0
        self.old_particles = None

        # Create a button to select and load tm_param.star files
        tm_params_btn = QPushButton("Select tm_param.star file(s)...")
        tm_params_btn.clicked.connect(self.on_load_tm_param_click)

        # Create a title for the list widget
        list_title = QLabel("Tomograms:")

        # Create a clickable list containing all tomogram numbers
        self.list_widget = QListWidget()
        self.list_widget.setFixedHeight(200)

        # Connect the itemClicked signal to a custom function
        self.list_widget.itemClicked.connect(self.on_tomo_item_click)

        # Create a Previous button and connect its clicked signal to a custom function
        self.prev_btn = QPushButton("Previous")
        self.prev_btn.clicked.connect(self.on_prev_click)

        # Create a Next button and connect its clicked signal to a custom function
        self.next_btn = QPushButton("Next")
        self.next_btn.clicked.connect(self.on_next_click)

        # Add the buttons to a horizontal layout
        hlayout = QHBoxLayout()
        hlayout.addWidget(self.prev_btn)
        hlayout.addWidget(self.next_btn)

        # Create a Matplotlib figure and an axes for the histogram plot
        plt.style.use('dark_background')
        self.fig = Figure()
        self.canvas = FigureCanvas(self.fig)
        #self.canvas.setFixedHeight(300)  # This apparently prevented it from working on some machines
        self.ax_hist = self.fig.add_axes([0.1, 0.2, 0.8, 0.7])

        # Create a QSlider widget and connect its valueChanged signal to a custom function
        self.threshold_slider = DoubleSlider(Qt.Horizontal)
        self.threshold_slider.doubleValueChanged.connect(self.on_threshold_slider_change)

        # Create a label for the threshold_slider and one for its value
        self.threshold_slider_label = QLabel("Threshold: ")
        self.threshold_slider_label.setBuddy(self.threshold_slider)
        self.threshold_spinbox = QDoubleSpinBox() 
        self.threshold_spinbox.setDecimals(4)
        self.threshold_spinbox.setSingleStep(0.005)  # set the increment step
        self.threshold_spinbox.valueChanged.connect(self.on_threshold_spinbox_change)

        # Add the QSlider widget and spacers to a horizontal layout
        hlayout_slider = QHBoxLayout()
        hlayout_slider.addWidget(self.threshold_slider_label)
        hlayout_slider.addWidget(self.threshold_spinbox)
        hlayout_slider.addWidget(self.threshold_slider)

        # Create the sigma slider
        self.sigma_slider = QSlider(Qt.Horizontal)
        self.sigma_slider.setRange(0, 100)
        self.sigma_slider.setSingleStep(1)  # Set the step size
        self.sigma_slider.valueChanged.connect(self.on_sigma_slider_change)  # Connect the valueChanged signal

        # Add a label for the sigma slider and one for its value
        self.sigma_slider_label = QLabel("n-th σ:")
        self.sigma_spinbox = QSpinBox()
        self.sigma_spinbox.setRange(0,100)
        self.sigma_spinbox.valueChanged.connect(self.on_sigma_spinbox_change)
        self.sigma_slider_label.setBuddy(self.sigma_slider)

        # Add the QSlider widget and spacers to a horizontal layout
        hlayout_sigma_slider = QHBoxLayout()
        hlayout_sigma_slider.addWidget(self.sigma_slider_label)
        hlayout_sigma_slider.addWidget(self.sigma_spinbox)
        hlayout_sigma_slider.addWidget(self.sigma_slider)

        # Change title of next section to "Particle Extraction Parameters"
        section_title_inputs = QLabel("Peak Extraction Parameters")

        # Add a label to show how many current particles there are
        self.particle_count_label = QLabel(f"Currently displayed particles: {0}")

        # Create a spin box for the peak extraction diameter input with placeholder text
        self.diameter_spinbox = QSpinBox()
        self.diameter_spinbox.setRange(1, 100)
        self.diameter_spinbox.setPrefix("Peak extraction diameter (px): ")
        # Set the size policy of the diameter spin box to expand horizontally
        self.diameter_spinbox.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)

        # Create a Button to reset any previously deleted (and remembered) points 
        self.reset_deleted_points_btn = QPushButton("Reset deleted points")
        self.reset_deleted_points_btn.clicked.connect(self.on_reset_deleted_points_click)
        # Set the size policy of the output line edit to not expand
        self.reset_deleted_points_btn.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)

        # Add the particle extraction inputs to a horizontal layout
        hlayout2 = QHBoxLayout()
        hlayout2.addWidget(self.diameter_spinbox)
        hlayout2.addWidget(self.reset_deleted_points_btn)

        # Create a Show Particles button and connect its clicked signal to a custom function
        self.show_particles_btn = QPushButton("Show Particles")
        self.show_particles_btn.clicked.connect(self.on_show_particles_click)

        # Change title of next section to "File Saving Parameters"
        section_title_output = QLabel("File Saving Parameters")

        # Float input field for pixel size
        self.px_size_input = QDoubleSpinBox()
        self.px_size_input.setDecimals(3)  # set the number of decimal places to 2
        self.px_size_input.setSingleStep(0.01)  # set the increment step
        self.px_size_input.setValue(1.000)  # set the default value
        self.px_size_input.setPrefix("Pixel size (Å): ")

        # Integer input field for binning
        self.binning_input = QSpinBox()
        self.binning_input.setSingleStep(1)  # set the increment step
        self.binning_input.setValue(1)  # set the default value
        self.binning_input.setPrefix("Binning: ")   

        # Add the previous two buttons to a horizontal layour
        hlayout3 = QHBoxLayout()
        hlayout3.addWidget(self.px_size_input)
        hlayout3.addWidget(self.binning_input)

        # Create a 'Save Current Particles' button and connect its clicked signal to a custom function
        self.save_particles_button = QPushButton("Save current particles")
        self.save_particles_button.clicked.connect(self.on_save_particles_click)

        # Create a 'Select Particle List Output Style' Dropdown widget. The first option is default.
        self.output_style_dropdown = QComboBox()
        self.output_style_dropdown.addItems(['stopgap', 'emmotl', 'relion4.0', 'relion3.1'])
        self.output_style_dropdown.setCurrentIndex(0)

        # Add the previous two buttons to a horizontal layour
        hlayout4 = QHBoxLayout()
        hlayout4.addWidget(self.save_particles_button)
        hlayout4.addWidget(self.output_style_dropdown)
        
        # Change title of next section to "Merge Individual Particle Lists"
        section_title_merge = QLabel("Merge Individual Particle Lists")

        # Create a Merge Particle Lists button and connect its clicked signal to a custom function
        self.merge_particle_lists_btn = QPushButton("Merge Particle Lists")
        self.merge_particle_lists_btn.clicked.connect(self.on_merge_particle_lists_click)

        # Add all widgets to the layout
        layout = QVBoxLayout()
        layout.addWidget(tm_params_btn)
        layout.addWidget(list_title)
        layout.addWidget(self.list_widget)
        layout.addLayout(hlayout)
        layout.addWidget(self.canvas)
        layout.addLayout(hlayout_slider)
        layout.addLayout(hlayout_sigma_slider)
        layout.addWidget(section_title_inputs)
        layout.addWidget(self.particle_count_label)
        layout.addLayout(hlayout2)
        layout.addWidget(self.show_particles_btn)
        layout.addWidget(section_title_output)
        layout.addLayout(hlayout3)
        layout.addLayout(hlayout4)
        layout.addWidget(section_title_merge)
        layout.addWidget(self.merge_particle_lists_btn)

        # Change the alignment and spacing of the layout
        layout.setAlignment(Qt.AlignTop)
        layout.setSpacing(10)

        # Add a stretchable space at the bottom of the layout
        spacer = QWidget()
        spacer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        layout.addWidget(spacer)

        self.setLayout(layout)

    def on_load_tm_param_click(self):
        # retrieve all file names of *.star files selected
        file_names_tuple = QFileDialog.getOpenFileNames(self, "Open Image", "C:/Users/schwarza/Nextcloud/Files/scripts/napari/napari-particle-extraction/data/", "tm_param Files (*.star)")
        # Extract the list of file names from the tuple
        file_names = file_names_tuple[0]

        # Check if any files were selected
        if not file_names:
            # No files were selected, so return early
            return

        # Assign value to tm_params instance variable
        result = napari_get_reader(file_names)

        # Check if a reader function was returned
        if callable(result):
            # Call the reader function to get the data
            self.tm_params = result(file_names)
        else:
            # The result is not callable, so assume it's the DataFrame directly
            self.tm_params = result

        # Extract the 'tomo_num' column from the dataframe
        tomo_nums = self.tm_params['tomo_num'].tolist()

        # Check for duplicates in the tomogram numbers
        if len(tomo_nums) != len(set(tomo_nums)):
            # Display an error message using QMessageBox
            QMessageBox.critical(
                self,
                'Error',
                'Duplicate tomogram numbers found!'
            )
            return
        # Update buttons to check if they are active
        self.update_buttons()

        # Clear the list widget
        self.list_widget.clear()

        # Add the tomogram numbers to the list widget
        for index, tomo_num in enumerate(tomo_nums):
            item = QListWidgetItem(str(tomo_num))
            item.setData(Qt.UserRole, index)
            self.list_widget.addItem(item)
    
    def on_tomo_item_click(self, item):
        # First delete all previous layers, save the viewer state
        self.viewer_angles = self.viewer.camera.angles
        self.viewer_zoom = self.viewer.camera.zoom
        self.viewer_center = self.viewer.camera.center
        self.last_ndisplay = self.viewer.dims.ndisplay
        self.last_grid_mode = self.viewer.grid.enabled
        self.current_dim_order = self.viewer.dims.order
        # Set all layers to None to free up memory and then remove them
        # Loop through a copy of the layers and remove them
        for layer in list(self.viewer.layers):
            self.viewer.layers.remove(layer)

        if hasattr(self, 'particles'):
            del self.particles
        if hasattr(self, 'old_particles'):
            del self.old_particles

        # Force garbage collection
        gc.collect()

        # Get the associated information (i.e. idx) of the clicked item
        item = self.list_widget.currentItem()
        idx = item.data(Qt.UserRole)
        item_text = item.text()
        tomo_path = self.tm_params.loc[idx, 'tomo_name']

        # Load tomogram
        self.load_volume(item_text, tomo_path, "Tomogram", "tomo_name")

        # Load score volume
        wd = self.tm_params.loc[self.tm_params['tomo_num'] == int(item_text),'rootdir'].iloc[0]
        smap_name = self.tm_params.loc[self.tm_params['tomo_num'] == int(item_text),'smap_name'].iloc[0]
        score_path = wd + '/outputs/' + smap_name + '_' + item_text + '.em'
        self.load_volume(item_text, score_path, "Score", "rootdir")

        # Load angles volume
        wd = self.tm_params.loc[self.tm_params['tomo_num'] == int(item_text),'rootdir'].iloc[0]
        omap_name = self.tm_params.loc[self.tm_params['tomo_num'] == int(item_text),'omap_name'].iloc[0]
        angles_path = wd + '/outputs/' + omap_name + '_' + item_text + '.em'
        self.load_volume(item_text, angles_path, "Angles", "rootdir")

        # Update buttons to check if they are active
        self.update_buttons()

        # Apply the saved ndisplay, grid mode, dimension order, and viewer states
        self.viewer.dims.ndisplay = self.last_ndisplay
        self.viewer.grid.enabled = self.last_grid_mode
        self.viewer.dims.order = self.current_dim_order
        self.viewer.camera.angles = self.viewer_angles
        self.viewer.camera.zoom = self.viewer_zoom
        self.viewer.camera.center = self.viewer_center

    def load_volume(self, item_text, path, name, update_column):
        # Submit this path to _reader.get_reader
        volume = napari_get_reader(path)
        show_file_not_found_dialog = True

        while volume is None:
            if show_file_not_found_dialog:
                if name in ["Score", "Angles"]:
                    result = QMessageBox.warning(
                    self,
                    'File not found',
                    f"The {name} volume could not be found. Please navigate to its location. Note: For scores and angles files, the convention is to store them in: [wd] / [outputs] / [smap_name] OR [angles_name] / [tomo_num] / ['.em'] OR ['.mrc'].\n\nPlease prepare your files accordingly.",
                    QMessageBox.Ok | QMessageBox.Cancel,
                    )
                else:
                    result = QMessageBox.warning(
                        self,
                        'File not found',
                        f"The {name} file could not be found. Would you like to navigate to its location?",
                        QMessageBox.Yes | QMessageBox.No,
                    )
            else:
                result = QMessageBox.Ok

            if result == QMessageBox.Ok or result == QMessageBox.Yes:
                file_name, _ = QFileDialog.getOpenFileName(
                    self,
                    'Select File',
                    '',
                    'All Files (*)'
                )

                if file_name:
                    path = file_name
                    volume = napari_get_reader(path)

                    if volume is None:
                        from _reader import file_endings

                        result = QMessageBox.warning(
                            self,
                            'Incompatible File Type',
                            "It looks like this file type is incompatible with the supported types {}. Do you want to try again?".format(file_endings),
                            QMessageBox.Ok | QMessageBox.Cancel,
                        )

                        if result == QMessageBox.Cancel:
                            break
                        else:
                            show_file_not_found_dialog = False
                    else:
                        result = QMessageBox.question(
                            self,
                            'Update file paths',
                            f'Should I update the {update_column} (in the tm_params.star file) for all entries in this list to point to this directory?',
                            QMessageBox.Yes | QMessageBox.No,
                        )

                        if result == QMessageBox.Yes:
                            if update_column == 'rootdir':
                                new_dir = os.path.dirname(os.path.dirname(path))
                                self.tm_params[update_column] = new_dir
                            else:
                                new_dir = os.path.dirname(path)
                                self.tm_params[update_column] = self.tm_params[update_column].apply(lambda x: os.path.join(new_dir, os.path.basename(x)))
                else:
                    break
            else:
                break

        volume_reader_function = napari_get_reader(path)

        if volume_reader_function is not None:
            # Retrieve the data, kwargs, layer_type and unpack tuple and list to get to volume data
            volume = volume_reader_function(path)[0][0]
            if name == "Score":
                self.score_name = f"{name} #{item_text}"
                self.score_min = volume.min()
                self.score_max = volume.max()
                cmap = self.hist_cmap
                new_layer = self.viewer.add_image(volume, name=self.score_name, colormap = cmap)

                # Connect the on_toggle_visibility method to the visible event of the new layer
                new_layer.events.visible.connect(lambda e: self.on_toggle_visibility(new_layer))
                # Update the name of the last added layer for this type
                self.layer_visibility[name]['name'] = self.score_name
                # Apply the stored visibility state
                self.viewer.layers[self.score_name].visible = self.layer_visibility[name]['visible']

                # Set the maximum to 1000x the actual value to work around floating point numbers
                self.threshold_slider.setMaximum(volume.max())

                # If the volume is a "Score" volume, initialize the histogram widget
                self.init_hist(volume, self.score_name)

            elif name == "Tomogram":
                self.tomo_name = f"{name} #{item_text}"
                cmap = 'gray'
                new_layer = self.viewer.add_image(volume, name=self.tomo_name, colormap = cmap)

                # Connect the on_toggle_visibility method to the visible event of the new layer
                new_layer.events.visible.connect(lambda e: self.on_toggle_visibility(new_layer))
                # Update the name of the last added layer for this type
                self.layer_visibility[name]['name'] = self.tomo_name
                # Apply the stored visibility state
                self.viewer.layers[self.tomo_name].visible = self.layer_visibility[name]['visible']

            elif name == "Angles":
                self.angles_name = f"{name} #{item_text}"
                cmap = 'gray'
                new_layer = self.viewer.add_image(volume, name=self.angles_name, colormap = cmap)

                # Connect the on_toggle_visibility method to the visible event of the new layer
                new_layer.events.visible.connect(lambda e: self.on_toggle_visibility(new_layer))
                # Update the name of the last added layer for this type
                self.layer_visibility[name]['name'] = self.angles_name                
                # Apply the stored visibility state
                self.viewer.layers[self.angles_name].visible = self.layer_visibility[name]['visible']


        return volume
    
    def init_hist(self, data, name):
        # Calculate the mean and standard deviation of the data
        # We calculate the std with n-1 here to control for the fact that we are sampling (since it's an image), i.e. "Bessel's correction"
        # Note: KDE and GMM were way too slow (even with 1% of the data), cutting off the top 10% and fitting the remainder
        # did not fit well, and truncated mean, windsored mean, median all looked highly similar. So I stuck with mean + x * std.
        self.score_mean = data.mean()
        self.score_std = data.std(ddof=1) 
        self.cmin, self.cmax = self.score_min, self.score_max 

        # Filter the data to remove zero values
        data = data[data != 0]

        # Clear the previous histogram plot
        self.ax_hist.clear()

        # Compute the histogram of non-zero values of the data
        n, self.bins, self.patches = self.ax_hist.hist(data.ravel(), bins=256, edgecolor='#e0e0e0', linewidth=0.2)
        # Store the original heights of the patches and the bin centers
        self.patch_heights = n

        # # Compute the assumed Gaussian distribution for visual inspection
        # bin_centers = 0.5 * (self.bins[:-1] + self.bins[1:])
        # y_fit = scipynorm.pdf(bin_centers, self.score_mean, self.score_std) * len(data) * (self.bins[1] - self.bins[0])

        # # Plot the assumed Gaussian
        # self.ax_hist.plot(bin_centers, y_fit, color='red', label='Assumed Gaussian', zorder=10)
        # self.ax_hist.legend()

        # Set the x/y-axis limits, title, name
        self.ax_hist.set_yscale('log')
        self.ax_hist.set_xlabel("Pixel value")
        self.ax_hist.set_ylabel("Count")
        self.ax_hist.set_title(name)

        # Update the slider value. This will also invoke update_hist() immediately to color the histogram.
        self.sigma_slider.setValue(0)
        self.update_hist()

    def on_reset_deleted_points_click(self):
        # Check if the deleted points layer exists
        if self.deleted_particles_name in self.viewer.layers:
            # Get the deleted points layer
            deleted_points_layer = self.viewer.layers[self.deleted_particles_name]

            # Check if the particles layer exists
            if self.current_particles_name in self.viewer.layers:
                # Get the particles layer
                particles_layer = self.viewer.layers[self.current_particles_name]

                # Add the deleted points back to the particles layer
                particles_layer.data = np.vstack([particles_layer.data, deleted_points_layer.data])

            # Delete the deleted points layer
            self.viewer.layers.remove(deleted_points_layer)

            # Update the particle count
            self.update_particle_count()

    def update_hist(self):

        # Update the histogram plot
        if hasattr(self, 'patch_heights'):
            cmap = plt.cm.get_cmap(self.hist_cmap)
            norm = plt.Normalize(self.cmin, max(self.bins))

            for i, patch in enumerate(self.patches):
                if patch.xy[0] < self.cmin:
                    patch.set_facecolor(cmap(0))
                    patch.set_alpha(0.3)
                else:
                    patch.set_facecolor(cmap(norm(self.bins[i])))
                    patch.set_alpha(0.8)
            self.canvas.draw()

    def update_contrast_limits(self, value):
        layer = self.viewer.layers[self.score_name]
        self.cmin = value
        self.cmin = min(self.cmin, self.cmax - 1e-10)  # Ensure cmin is less than cmax
        try:
            layer.contrast_limits = (self.cmin, self.cmax + 1e-10)  # Add a small offset to avoid problems at high values
        except RuntimeError as r:
            # This error is caused when trying to change contrast limits in different dimensionalities (by vispy)
            print(f"An error occurred in _widget.py: {r}. Not sure how to fix it yet.")

    def on_threshold_slider_change(self, value):
        # Update the threshold QDoubleSpinBox value
        self.threshold_spinbox.setValue(value) # This will trigger a slider change again, but no idea how to prevent it
        
    def on_threshold_spinbox_change(self, value):

        # Disconnect the signals to prevent an infinite loop
        self.threshold_slider.doubleValueChanged.disconnect()
        self.threshold_spinbox.valueChanged.disconnect()

        self.threshold_slider.setValue(value)

        self.update_contrast_limits(value)
        self.update_hist()

        # Reconnect the signals
        self.threshold_slider.doubleValueChanged.connect(self.on_threshold_slider_change)
        self.threshold_spinbox.valueChanged.connect(self.on_threshold_spinbox_change)

    def on_sigma_slider_change(self, value):
        # Disconnect the signals to prevent an infinite loop
        self.sigma_slider.valueChanged.disconnect()
        self.sigma_spinbox.valueChanged.disconnect()

        new_threshold = self.score_mean + value * self.score_std

        self.threshold_spinbox.setValue(new_threshold) # This in turn will trigger updating the threshold QSlider
        self.sigma_spinbox.setValue(value)  # This will trigger a slider change again, but no idea how to prevent it

        # Reconnect the signals
        self.sigma_slider.valueChanged.connect(self.on_sigma_slider_change)
        self.sigma_spinbox.valueChanged.connect(self.on_sigma_spinbox_change)

    def on_sigma_spinbox_change(self, value):
        self.sigma_slider.setValue(value)
        self.on_sigma_slider_change(value)

    def on_prev_click(self):
        # Get the index of the currently selected item in the list widget
        current_row = self.list_widget.currentRow()

        # Check if there is a previous item in the list widget
        if current_row > 0:
            # Select the previous item in the list widget
            prev_row = current_row - 1
            prev_item = self.list_widget.item(prev_row)
            self.list_widget.setCurrentItem(prev_item)

            # Trigger the itemClicked signal of the list widget to update the plot
            self.list_widget.itemClicked.emit(prev_item)

    def on_next_click(self):
        # Get the index of the currently selected item in the list widget
        current_row = self.list_widget.currentRow()

        # Check if there is a next item in the list widget
        if current_row < self.list_widget.count() - 1:
            # Select the next item in the list widget
            next_row = current_row + 1
            next_item = self.list_widget.item(next_row)
            self.list_widget.setCurrentItem(next_item)

            # Trigger the itemClicked signal of the list widget to update the plot
            self.list_widget.itemClicked.emit(next_item)

    def update_buttons(self):
        # Get the index of the currently selected item in the list widget
        current_row = self.list_widget.currentRow()

        # Enable or disable the Previous button depending on whether there is a previous item in the list widget
        self.prev_btn.setEnabled(current_row > 0)

        # Enable or disable the Next button depending on whether there is a next item in the list widget
        self.next_btn.setEnabled(current_row < self.list_widget.count() - 1)

    def on_show_particles_click(self):
        # Get the particle diameter from the diameter spin box
        particle_diameter = self.diameter_spinbox.value()

        # Get the threshold values from the threshold spin box
        threshold = self.threshold_spinbox.value()

        # If threshold is very low, ask if that was actually intended
        if threshold <= (self.score_mean + 5*self.score_std):
            result = QMessageBox.warning(
                self,
                "Are you sure?",
                "The threshold that is currently selected ({}) would result in a long run time. Are you sure you want to proceed?".format(threshold),
                QMessageBox.Yes | QMessageBox.Cancel,
                )
            
            if result == QMessageBox.Cancel:
                return

        # Get the specific entry (pd.Series) from tm_params dataframe to be processed.
        tm_param = self.tm_params.loc[self.list_widget.currentRow(),:]

        # Get the data from the currently loaded 'score_name' image layer
        score_volume = self.viewer.layers[self.score_name].data
        angles_volume = self.viewer.layers[self.angles_name].data

        # Import the sg_tm_extract_particles function from _sg_tm_extract_particles.py
        from napari_particle_extraction._sg_tm_extract_particles import sg_tm_extract_particles

        # Call the sg_tm_extract_particles function with the gathered input and data
        try:
            self.particles = sg_tm_extract_particles(self.viewer, tm_param, score_volume, angles_volume, particle_diameter, threshold)
        except ValueError as e:
            # Handle the error
            print(f"An error occurred in _sg_tm_extract_particles.py: {e}")

        # If None is returned, wipe the current points layer clean
        if self.particles is None:
            all_particles = np.empty((0, 3))  # Empty array with 3 columns
            properties = None
        else:
            # Filter out the deleted particles and prepare properties
            all_particles = self.particles.loc[:, ('orig_z', 'orig_y', 'orig_x')]
            if self.deleted_particles_name in self.viewer.layers:
                deleted_particles = self.viewer.layers[self.deleted_particles_name].data
                is_deleted = np.isin(all_particles, deleted_particles).all(axis=1)
                all_particles = all_particles[~is_deleted]
                self.particles = self.particles.loc[~is_deleted]

            properties = self.particles.to_dict(orient='list')

        # Check if the self.current_particles_name layer exists in the viewer
        points_layer = next((layer for layer in self.viewer.layers if layer.name == self.current_particles_name), None)

        # If the layer exists, update it
        if points_layer is not None:
            points_layer.data = all_particles
            if self.particles is not None:
                points_layer.size = particle_diameter
                points_layer.properties = properties
                points_layer.edge_color = 'score'
                points_layer.edge_colormap = 'viridis'
                points_layer.edge_contrast_limits = (0, self.cmax)

            else:
                # If no particles were added, show a warning
                QMessageBox.warning(self,
                                    'No particles added',
                                    f"No particles were found with these settings.",
                                    QMessageBox.Ok)
            self.update_particle_count()

        # If no layer exists and particles are found, create the layer
        elif points_layer is None and np.size(all_particles):
            points_layer = self.viewer.add_points(all_particles,
                                                size=particle_diameter,
                                                opacity=1.0,
                                                properties=properties,
                                                edge_color='score',
                                                edge_colormap='viridis',
                                                edge_contrast_limits=(0, self.cmax),
                                                edge_width=0.3,
                                                face_color='transparent',
                                                ndim=3,
                                                n_dimensional=True,
                                                name=self.current_particles_name)
            self.update_particle_count()
            self.viewer.layers[self.current_particles_name].events.data.connect(self.on_points_data_changed)

        # If no layer exists and no particles were found, create an empty layer and show a warning
        else:
            QMessageBox.warning(self,
                                'No particles added',
                                f"No particles were found with these settings.",
                                QMessageBox.Ok)
            return
        
        # Get the 'old_particles' to compare against potential new ones
        self.old_particles = points_layer.data
        self.particle_count = len(points_layer.data)

        # Filter out deleted particles
        if self.deleted_particles_name in self.viewer.layers:
            deleted_particles = self.viewer.layers[self.deleted_particles_name].data
            current_particles = points_layer.data
            current_particles = current_particles[~np.isin(current_particles, deleted_particles).all(axis=1)]
            points_layer.data = current_particles
            
    def on_save_particles_click(self):
        # Check if the self.current_particles_name layer exists
        layer = next((layer for layer in self.viewer.layers if layer.name == self.current_particles_name), None)

        # Get pixel size and binning values from input fields
        px_size = self.px_size_input.value()
        binning = self.binning_input.value()

        if layer is not None:
            # Get the properties of the points in the layer
            properties = layer.properties

            # Convert the properties from a dictionary to a DataFrame
            df = pd.DataFrame(properties)
            # Reset the index, _motl_idx and _subtomo_num
            df.reset_index(drop = True, inplace = True)
            df['subtomo_num'] = np.arange(1, len(df) + 1)

            # Shift all particle positions by +1 to convert from 'start at 0' in python
            # to 'start at 1' in stopgap, relion?, tomsa, novaSTA etc.
            df[['orig_x', 'orig_y', 'orig_z']] += 1

            # Get the selected output style from the dropdown menu
            output_style = self.output_style_dropdown.currentText()

            # Create a suggested file name
            suggested_filename = f"{output_style}_particles_{self.list_widget.currentItem().text()}"

            # Determine the file type based on the output style
            if output_style in ['stopgap', 'relion4.0', 'relion3.1']:
                suggested_filename += ".star"
                file_type = "Star Files (*.star)"
            elif output_style == 'emmotl':
                suggested_filename += ".em"
                file_type = "EM Files (*.em)"
            else:
                file_type = "All Files (*)"

            # Open a QFileDialog to choose the save location
            options = QFileDialog.Options()
            filename, _ = QFileDialog.getSaveFileName(self,"QFileDialog.getSaveFileName()", suggested_filename, file_type, options=options)
            
            if filename:
                # Call the function to save the DataFrame
                success = napari_get_writer(df, output_style, filename, px_size, binning)

                # If the save was successful, change the background color of the corresponding QListWidget item
                if success:
                    current_item = self.list_widget.currentItem()
                    green_color = QColor(34, 187, 69)
                    current_item.setBackground(QBrush(green_color))
                    # Update the item to trigger a repaint
                    row = self.list_widget.row(current_item)
                    self.list_widget.update(self.list_widget.model().index(row, 0))

    def on_merge_particle_lists_click(self):

        # Get pixel size and binning values from input fields
        px_size = self.px_size_input.value()
        binning = self.binning_input.value()

        options = QFileDialog.Options()
        options |= QFileDialog.ReadOnly

        # Open a File selection dialog to open multiple .em or .star files
        files, _ = QFileDialog.getOpenFileNames(
            self,
            "Select one or more particle list files to merge",
            "",
            "Particle List Files (*.em *.star)",
            options=options
        )
        
        if not files:
            return

        # Call the _reader.napari_get_reader function with the list of paths
        # Transform and merge into a single pandas DataFrame if not already
        merged_df = pd.concat([napari_get_reader(file) for file in files], ignore_index = True)
        merged_df['motl_idx'] = range(1, len(merged_df) + 1)
        merged_df['subtomo_num'] = range(1, len(merged_df) + 1)
        
        # Get the selected output style from the dropdown menu
        output_style = self.output_style_dropdown.currentText()

        # Create a suggested file name
        suggested_filename = f"{output_style}_merged_particles"

        # Determine the file type based on the output style
        if output_style in ['stopgap', 'relion4.0', 'relion3.1']:
            suggested_filename += ".star"
            file_type = "Star Files (*.star)"
        elif output_style == 'emmotl':
            suggested_filename += ".em"
            file_type = "EM Files (*.em)"
        else:
            file_type = "All Files (*)"

        # Open a QFileDialog to choose the save location
        options = QFileDialog.Options()
        save_path, _ = QFileDialog.getSaveFileName(self, "Save Merged Particle List", suggested_filename, file_type, options=options)

        if not save_path:
            return

        # Submit the DataFrame, the path, px_size and binning to the napari_get_writer() function in _writer
        napari_get_writer(merged_df, self.output_style_dropdown.currentText(), save_path, px_size, binning)

    def update_particle_count(self):
        count = len(self.viewer.layers[self.current_particles_name].data)
        self.particle_count_label.setText(f"Currently displayed particles: {count}")

    def on_points_data_changed(self, event):
        # Get the particles layer
        points_layer = self.viewer.layers[self.current_particles_name]
        new_particle_count = points_layer.data.shape[0]

        # Check if the deleted points layer exists
        if self.deleted_particles_name not in self.viewer.layers:
            # If not, create it with the correct dimensionality and appearance
            deleted_particles_layer = self.viewer.add_points(ndim = 3,
                                                            opacity=1.0, 
                                                            name=self.deleted_particles_name, 
                                                            size = self.diameter_spinbox.value(),
                                                            edge_width = 0.3,
                                                            edge_color = 'grey',
                                                            edge_contrast_limits = (0, self.cmax),
                                                            face_color = 'transparent',
                                                            n_dimensional = True)
        else:
            deleted_particles_layer = self.viewer.layers[self.deleted_particles_name]

        if new_particle_count < self.particle_count:
            # A particle was deleted
            deleted_particles = self.old_particles[~np.isin(self.old_particles, points_layer.data).all(axis=1)]
            if deleted_particles_layer.data.shape[0] == 0:
                # If the deleted particles layer is empty, just replace the data
                deleted_particles_layer.data = deleted_particles
            else:
                deleted_particles_layer.data = np.vstack([self.viewer.layers[self.deleted_particles_name].data, deleted_particles])

        self.old_particles = points_layer.data
        self.particle_count = new_particle_count

        # Select the points layer again and de-select all newly added particles
        self.viewer.layers.selection.active = deleted_particles_layer
        deleted_particles_layer.selected_data = []
        self.viewer.layers.selection.active = points_layer
        points_layer.selected_data = []

        self.update_particle_count()

    def on_toggle_visibility(self, layer):
        # Find the type of the toggled layer by its name
        layer_type = [key for key, value in self.layer_visibility.items() if value['name'] == layer.name][0]
        
        # Update the visibility state
        self.layer_visibility[layer_type]['visible'] = layer.visible
    
class DoubleSlider(QSlider):

    doubleValueChanged = Signal(float)  # Custom signal to emit float value

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.decimals = 5
        self._max_int = 10 ** self.decimals

        super().setMinimum(0)
        super().setMaximum(self._max_int)

        self._min_value = 0.0
        self._max_value = 1.0

        # Connect the built-in valueChanged signal to a custom slot
        self.valueChanged.connect(self._emit_double_value_changed)

    @property
    def _value_range(self):
        return self._max_value - self._min_value

    def value(self):
        return float(super().value()) / self._max_int * self._value_range + self._min_value

    def setValue(self, value):
        int_value = int((value - self._min_value) / self._value_range * self._max_int)
        super().setValue(int_value)

    def setMinimum(self, value):
        if value > self._max_value:
            raise ValueError("Minimum limit cannot be higher than maximum")

        self._min_value = value

    def setMaximum(self, value):
        if value < self._min_value:
            raise ValueError("Minimum limit cannot be higher than maximum")

        self._max_value = value

    def minimum(self):
        return self._min_value

    def maximum(self):
        return self._max_value
    
    def _emit_double_value_changed(self):
        # Emit the custom signal with the correct float value
        self.doubleValueChanged.emit(self.value())

def napari_experimental_provide_widget():
    # you can return either a single widget, or a sequence of widgets
    return SG_TM_Particle_Extract

if __name__ == "__main__":
    viewer = napari.Viewer()
    napari.run()