# -*- coding: utf-8 -*-
"""
Created on Tue Jul  5 11:59:30 2022

Generate a STOPGAP motivelist from the output of a STOPGAP Template
Matching run. Additional parameters are the score threshold and distance
threshold.

This script is based on a script from the STOPGAP toolbox by William Wan (@Vanderbilt)
(https://github.com/williamnwan/STOPGAP) for identification of particles from STOPGAP
template matching CC score and production of a star file containing those particles.
It was here translated to python and modified for use within a plugin in Napari.

@author: Andre Schwarz, MPI Brain, Frankfurt am Main, Germany
andre.schwarz@brain.mpg.de

"""
import gc
from scipy.spatial import cKDTree
from sklearn.cluster import DBSCAN
import pandas as pd
import numpy as np
import napari
import emfile.read as emread
import starfile


def sg_tm_extract_particles(viewer: "napari.Viewer", tm_param, score_volume, angles_volume, particle_diameter: int, threshold: float, cluster_size: int = None, n_particles = None):

    # Read angle list.
    anglist = pd.read_csv(tm_param['rootdir'] + '/inputs/' + tm_param['anglist_name'], header=None, names=['phi', 'psi', 'the'])
    
    # Threshold and sort indices/scores
    t_idx = np.where(score_volume > threshold)

    if n_particles is not None:
        k = min(n_particles, len(t_idx[0]))
    else:
        k = len(t_idx[0])

    # Check for early termination
    if k == 0:
        return None

    k = min(k, len(score_volume[t_idx])) - 1
    s_idx = np.argpartition(-score_volume[t_idx], k)[:k+1]
    s_idx = s_idx[np.argsort(-score_volume[t_idx][s_idx])] # Sort for later

    # Sorted indices. s_ind[0] = x, s_ind[1] = y, s_ind[2] = z
    s_ind = np.array([t_idx[0][s_idx], t_idx[1][s_idx], t_idx[2][s_idx]])
    n_vox = len(s_idx)

    # Create a list of tuples where each tuple is (coord, score) and sort it by score in descending order
    scored_coords = sorted(zip(s_ind.T, score_volume[s_ind[0], s_ind[1], s_ind[2]]), key=lambda x: x[1], reverse=True)

    # Build a KD-tree with the coordinates
    tree = cKDTree([coord for coord, score in scored_coords])

    # Remove any points that are within the specified particle diameter of a higher score point
    coord_to_score = {tuple(coord): score for coord, score in scored_coords}
    remaining_coords = set(coord_to_score.keys())
    filtered_coords = []
    
    for coord, score in scored_coords:
        if tuple(coord) not in remaining_coords:
            continue
        filtered_coords.append((coord, score))
        nearby_coords = tree.query_ball_point(coord, particle_diameter)
        for nearby_coord in nearby_coords:
            nearby_coord_tuple = tuple(scored_coords[nearby_coord][0])
            if nearby_coord_tuple in remaining_coords and coord_to_score[nearby_coord_tuple] <= score:
                remaining_coords.remove(nearby_coord_tuple)

    # Extract the coordinates from the filtered_coords list
    filtered_coords, filtered_scores = zip(*filtered_coords)
    filtered_coords = np.array(filtered_coords)
    filtered_scores = np.array(filtered_scores)

    # Use DBSCAN to cluster points
    clusterer = DBSCAN(eps=particle_diameter/2, min_samples=1)
    cluster_labels = clusterer.fit_predict(filtered_coords)

    # Keep track of hits in case of number of particles
    filtered_hit_idx = np.zeros(len(filtered_coords), dtype=bool)

    # Count number of hits
    c = 0
    for cluster_id in np.unique(cluster_labels):
        if cluster_id == -1:
            continue

        # Check cluster size
        if cluster_size is not None:
            c_size = np.sum(cluster_labels == cluster_id)
            if c_size < cluster_size:
                continue

        filtered_hit_idx[cluster_labels == cluster_id] = True
        c += np.sum(cluster_labels == cluster_id)

        # Check for early termination
        if n_particles is not None and c >= n_particles:
            break

    # Remaining positions
    rpos = filtered_coords[filtered_hit_idx].T
    n_pos = np.sum(filtered_hit_idx)

    ##### Generate motivelist #####
    print('Generating motivelist...')

    # Initialize/fill motivelist
    motl = pd.DataFrame(data={#'motl_idx': range(1, n_pos + 1),
                                        'tomo_num': tm_param['tomo_num'],
                                        'object': 1,
                                        'subtomo_num': range(1, n_pos + 1),
                                        'halfset': 'A',
                                        'orig_x': rpos[2, :],   # Note that this is python convention starting at 0
                                        'orig_y': rpos[1, :],   # Note that this is python convention starting at 0
                                        'orig_z': rpos[0, :],   # Note that this is python convention starting at 0
                                        'score': 0,
                                        'x_shift': 0,
                                        'y_shift': 0,
                                        'z_shift': 0,
                                        'phi': 0,
                                        'psi': 0,
                                        'the': 0,
                                        'class': 1})

    # Fill orientation and scores
    # Parse angle index
    ang_idx = angles_volume[rpos[0, :], rpos[1, :], rpos[2, :]].astype(int)
    motl.loc[:,'phi'] = anglist.loc[ang_idx, 'phi'].reset_index(drop=True) # Note that this is python convention starting at 0
    motl.loc[:,'psi'] = anglist.loc[ang_idx, 'psi'].reset_index(drop=True) # Note that this is python convention starting at 0
    motl.loc[:,'the'] = anglist.loc[ang_idx, 'the'].reset_index(drop=True) # Note that this is python convention starting at 0
    # Parse score
    motl.loc[:,'score'] = filtered_scores

    # ToDo: Randomize euler angles by symmetry. Skipped for now, only applicable for c1. Write out warning instead.
    # motl = sg_motl_randomize_eulers_by_symmetry(motl, tlist.loc[0, 'symmetry'])
    if tm_param['symmetry'].lower() != 'c1':
        raise ValueError("Currently only 'c1' symmetry is supported")
    del s_ind, scored_coords
    gc.collect()

    return motl

if __name__ == '__main__':
    viewer = napari.Viewer()
    # napari.run()
    tm_param = starfile.read('C:/Users/schwarza/Desktop/X_tm_param_CR_063.star').loc[0,:]
    score_volume = emread('C:/Users/schwarza/Desktop/outputs/scores_CR_63.em')[1]
    angles_volume = emread('C:/Users/schwarza/Desktop/outputs/angles_CR_63.em')[1]
    particle_diameter = 10
    threshold = 0.055
    motl = sg_tm_extract_particles(viewer, tm_param, score_volume, angles_volume, particle_diameter, threshold)
    print(motl)
