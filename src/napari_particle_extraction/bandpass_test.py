# tomograms to test with: 03037, 03089, 03098, !04012, !04014, !04015, 04017, 04021, 04022, !04030, !04031
# 04036, 04037, !04042, 04054, !!!04102, !04112, !04113, !04114, !!!05063, !!!05145, !!!05178, !!!05179
import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter
from scipy.fft import ifftn
import emfile
from mpl_toolkits.mplot3d import Axes3D

def create_bandpass_filter(shape, low_cutoff, high_cutoff):
    z, y, x = np.meshgrid(np.fft.fftfreq(shape[0]), np.fft.fftfreq(shape[1]), np.fft.fftfreq(shape[2]), indexing='ij')
    radial_freq = np.sqrt(x**2 + y**2 + z**2)
    mask = (radial_freq >= low_cutoff) & (radial_freq <= high_cutoff)
    mask = gaussian_filter(mask.astype(float), sigma=1)
    return mask

def apply_bandpass_filter(volume, bandpass_filter):
    fft_volume = np.fft.fftn(volume)
    filtered_fft_volume = fft_volume * bandpass_filter
    filtered_volume = np.real(ifftn(filtered_fft_volume))
    return filtered_volume

def plot_surface(ax, data, title):
    X, Y = np.meshgrid(np.arange(data.shape[1]), np.arange(data.shape[0]))
    ax.plot_surface(X, Y, data, cmap='viridis', rstride=1, cstride=1, linewidth=0, antialiased=False)
    ax.set_title(title)
    ax.axis('off')

import mrcfile
# Load the further cropped volume
further_cropped_volume = mrcfile.read("C:/Users/schwarza/Nextcloud/Files/scripts/napari/napari-particle-extraction/data/cropped_volume.em")

filter_shape = further_cropped_volume.shape

# Define a range of variations for the bandpass filter
variations_around_initial = [
    (0.05, 0.15), (0.1, 0.2), (0.15, 0.25), (0.05, 0.25) # Different frequency ranges
]

filtered_mips_new = []

# Apply the new variations and compute the MIP renderings
for variation in variations_around_initial:
    low_cutoff_var, high_cutoff_var = variation
    bandpass_filter_var = create_bandpass_filter(filter_shape, low_cutoff_var, high_cutoff_var)
    filtered_volume_var = apply_bandpass_filter(further_cropped_volume, bandpass_filter_var)
    mip_filtered_volume_var = np.max(filtered_volume_var, axis=0)
    filtered_mips_new.append(mip_filtered_volume_var)

mip_original = np.max(further_cropped_volume, axis=0)

fig, axes = plt.subplots(2, 3, figsize=(18, 12))
axes[0, 0].imshow(mip_original, cmap='viridis')
axes[0, 0].set_title('Original MIP')
axes[0, 0].axis('off')

for ax, mip, variation in zip(axes.flat[1:], filtered_mips_new, variations_around_initial):
    ax.imshow(mip, cmap='viridis')
    ax.set_title(f'Filtered MIP\n{variation}')
    ax.axis('off')

plt.tight_layout()
plt.show()

fig = plt.figure(figsize=(18, 12))
ax_original = fig.add_subplot(2, 3, 1, projection='3d')
plot_surface(ax_original, mip_original, 'Original MIP')

for i, (mip, variation) in enumerate(zip(filtered_mips_new, variations_around_initial), 2):
    ax_filtered = fig.add_subplot(2, 3, i, projection='3d')
    plot_surface(ax_filtered, mip, f'Filtered MIP\n{variation}')

plt.tight_layout()
plt.show()
a=6