# -*- coding: utf-8 -*-
"""
Created on 2022-08-15

Testing sg_tm_refresh_settings.py

@author: Andre Schwarz, MPI Brain, Frankfurt am Main, Germany
andre.schwarz@brain.mpg.de
"""
import pytest

from _sg_tm_refresh_settings import sg_tm_refresh_settings

