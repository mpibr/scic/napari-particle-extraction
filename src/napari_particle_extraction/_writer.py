"""
This module is an example of a barebones writer plugin for napari

Replace code below according to your needs
"""

import pandas as pd
from cryocat import cryomotl

def napari_get_writer(particle_list: pd.DataFrame, output_style: str, filename, px_size: float, binning):

    success = save_particle_list(particle_list, output_style, filename, px_size, binning)
    return success

def save_particle_list(particle_list: pd.DataFrame, output_style: str, filename, px_size: float, binning):

    motl = cryomotl.StopgapMotl(particle_list)
    print("motl type: ", type(motl))
    print("particle_list type: ", type(particle_list))

    try:
        if output_style == 'stopgap':
            # Convert (back) to stopgap and supply an output filename to write file
            motl.write_out(filename) # ToDo: It's broken. This needs updating to match the new cryocat format!!!
            print("saving ", filename, ", output_style: ", output_style)
            return True
            
        elif output_style == 'emmotl':
            # Don't have to convert, use built-in class function to write file
            cryomotl.stopgap2emmotl(motl.df, filename)
            print("saving ", filename, ", output_style: ", output_style)
            return True

        elif output_style == 'relion4.0':
            # Convert to relion4.0 and supply an output filename to write file
            # ToDo: For >=4.0, the coordinates have to be unbinned so you have to correctly speficy the binning so it is correclty multiplied. 
            cryomotl.stopgap2relion(motl.df, filename, relion_version = 4.0, pixel_size = px_size, binning = binning)
            print("saving ", filename, ", output_style: ", output_style)
            return True
        elif output_style == 'relion3.1':
            # Convert to relion3.1 and supply an output filename to write file
            cryomotl.stopgap2relion(motl.df, filename, relion_version = 3.1, pixel_size = px_size, binning = binning)
            print("saving ", filename, ", output_style: ", output_style)
            return True
        
    except Exception as e:
        # If an error occurred, print the error and return False
        print(f"Failed to save DataFrame: {e}")
        return False
